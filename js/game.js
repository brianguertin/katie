
var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, '', { preload: preload, create: create, update: update });

function preload() {
    game.load.image('sky', 'assets/sky.png');
    game.load.image('grass', 'assets/grass.png');
    game.load.image('heart', 'assets/heart.png');
    game.load.spritesheet('katie', 'assets/katie.png', 40, 40);
    game.load.spritesheet('chicken', 'assets/chicken.png', 32, 21);
    game.load.spritesheet('frog', 'assets/frog.png', 32, 23);
    game.load.spritesheet('bunny', 'assets/bunny.png', 32, 26);
    game.load.spritesheet('bird', 'assets/bird.png', 32, 27);
    game.load.spritesheet('chick', 'assets/chick.png', 32, 8);
    game.load.spritesheet('mouse', 'assets/mouse.png', 32, 13);
    game.load.audio('pop', ['assets/audio/pop.mp3', 'assets/audio/pop.ogg']);
    game.load.audio('meow', ['assets/audio/meow.mp3', 'assets/audio/meow.ogg']);
}

var player;
var platforms;
var cursors;

var stars;
var score = 0;
var scoreText;
var running = false;
var walking = false;
var popAudio;
var hitAudio;
var baddies;
var frogs;
var bunnies;
var spawnDistanceFromPlayerX = window.innerWidth;
var spawnIntervalX = 300;
var farthestSpawnX = spawnDistanceFromPlayerX;

function create() {
    popAudio = game.add.audio('pop',1,false);
    hitAudio = game.add.audio('meow',1,false);
    
    var worldWidth = 8000000;
    game.world.setBounds(0, 0, worldWidth, window.innerHeight);
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.arcade.gravity.y = 800;
    minSpawnX = window.innerWidth;
    
    //  A simple background for our game
    var sky = game.add.sprite(0, 0, 'sky');
    sky.scale.setTo(200, 4);
    //sky.body.allowGravity = false;

    //  The platforms group contains the ground and the 2 ledges we can jump on
    platforms = game.add.group();

    // Here we create the ground.
    //var ground = game.add.sprite(0, game.world.height - 15, 'grass');
    var ground = game.add.tileSprite(0, game.world.height - 16, worldWidth, 16, 'grass');
    game.physics.enable(ground, Phaser.Physics.ARCADE);
    //ground.body.setRectangle(worldWidth, 16, 0, 0);
    platforms.add(ground);

    //  This stops it from falling away when you jump on it
    ground.body.immovable = true;
    ground.body.allowGravity = false;

    //  Now let's create two ledges
    /*var ledge = platforms.create(400, 400, 'ground');
    ledge.body.immovable = true;

    ledge = platforms.create(-150, 250, 'ground');
    ledge.body.immovable = true;*/

    // The player and its settings
    player = game.add.sprite(32, game.world.height - 150, 'katie');
    game.physics.enable(player, Phaser.Physics.ARCADE);

    //  Player physics properties. Give the little guy a slight bounce.
    player.body.bounce.y = 0.01;
    //player.body.gravity.y = 800;
    player.body.collideWorldBounds = true;

    //  Our two animations, walking left and right.
    player.animations.add('walk', [3, 4], 10, true);
    player.animations.add('run', [1, 2], 10, true);

    baddies = game.add.group();
    
    frogs = game.add.group();
    //baddies.add(frogs); // Broken?
    bunnies = game.add.group();
    //baddies.add(bunnies);
    
    //  Finally some stars to collect
    stars = game.add.group();

    //  The score
    var scoreboard = game.add.sprite(0,0);
    scoreboard.fixedToCamera = true;
    scoreText = game.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });
    scoreboard.addChild(scoreText);
    //scoreboard.kill(); // TODO: put this back after the jittering is fixed

    //  Our controls.
    cursors = game.input.keyboard.createCursorKeys();
    
    game.camera.follow(player);
    
    // Keeps the player to the left side of the screen
    var w = game.camera.width / 8;
    var h = game.camera.height / 3;
    game.camera.deadzone = new Phaser.Rectangle(w, (game.camera.height - h) / 2 - h * 0.25, w / 2, h);
}

function spawn(x) {
    var rand = Math.random();
    if (rand > 0.9) {
        var chicken = baddies.create(x, game.world.height - 50, 'chicken');
        game.physics.enable(chicken, Phaser.Physics.ARCADE);
        chicken.animations.add('walk', [0, 1, 2, 1], 10, true);
        chicken.body.bounce.y = 0;
        chicken.animations.play('walk');

    } else if (rand > 0.7) {
        var frog = frogs.create(x, game.world.height - 50, 'frog');
        game.physics.enable(frog, Phaser.Physics.ARCADE);
        frog.body.bounce.y = 0.01;

    } else if (rand > 0.5) {
        var bird = baddies.create(x, game.world.height - 150, 'bird');
        game.physics.enable(bird, Phaser.Physics.ARCADE);
        bird.animations.add('walk', [0, 1, 2, 1], 10, true);
        bird.animations.add('fly', [3, 4, 5, 4], 10, true);
        bird.animations.play('fly');
        bird.body.bounce.y = 0.01;
        bird.body.allowGravity = false;

    } else if (rand > 0.2) {
        var bunny = bunnies.create(x, game.world.height - 50, 'bunny');
        game.physics.enable(bunny, Phaser.Physics.ARCADE);
        bunny.body.bounce.y = 0.01;

    } else {
        var star = stars.create(x, game.world.height - 50, 'heart');
        game.physics.enable(star, Phaser.Physics.ARCADE);
        //star.body.gravity.y = 300;
        star.body.bounce.y = 0;
    }
}

function update() {
    var nextSpawnX = farthestSpawnX + spawnIntervalX;
    if (nextSpawnX < player.body.x + spawnDistanceFromPlayerX) {
        spawn(nextSpawnX);
        farthestSpawnX = nextSpawnX;
    }
    
    game.physics.arcade.collide(player, platforms);
    game.physics.arcade.collide(stars, platforms);
    game.physics.arcade.collide(baddies, platforms);
    game.physics.arcade.collide(frogs, platforms);
    game.physics.arcade.collide(bunnies, platforms);

    game.physics.arcade.overlap(player, stars, collectStar, null, this);
    game.physics.arcade.overlap(player, baddies, touchBaddy, null, this);
    game.physics.arcade.overlap(player, frogs, touchBaddy, null, this);
    game.physics.arcade.overlap(player, bunnies, touchBaddy, null, this);
    
    frogs.forEachAlive(function(frog) {
        if (frog.body.touching.down && frog.body.x > player.body.x && frog.body.x < player.body.x + 100) {
            frog.body.velocity.y = -400;
        }
        if (frog.body.touching.down) {
            frog.frame = 1;
        } else {
            frog.frame = 0;
        }
    });
    
    bunnies.forEachAlive(function(bunny) {
        if (bunny.body.touching.down && !bunny.jumpTimer) {
            bunny.jumpTimer = game.time.events.add(Phaser.Timer.SECOND * 1, function(){
                bunny.body.velocity.y = -400;
                bunny.jumpTimer = null;
            }, this);
        }
        if (bunny.body.touching.down) {
            bunny.frame = 1;
        } else {
            bunny.frame = 0;
        }
    });
    
    
    if (player.body.touching.down) {
        if (running) {
            player.animations.play('run');
            player.body.velocity.x = 550; // FIXME: for debugging
        } else if (walking) {
            player.animations.play('walk');
            player.body.velocity.x = 250;
        } else {
            player.animations.stop();
            player.frame = 0;
            player.body.velocity.x = 0;
        }
    } else {
        //  Airborn
        player.animations.stop();
        player.frame = 1;
    }
    
    //  Allow the player to jump if they are touching the ground.
    if (player.body.touching.down && (cursors.up.isDown || game.input.mousePointer.isDown || game.input.pointer1.isDown)) {
        if (!walking) {
            walking = true;
        } else {
            player.body.velocity.y = -400;
        }
    }
    
    running = cursors.right.isDown;  // FIXME: for debugging
        
    
    // FIXME: for debugging
    if (cursors.down.isDown) {
        walking = false;
        running = false;
        player.body.velocity.x = -650;
    }
}

function collectStar (player, star) {
    star.kill();

    score += 10;
    scoreText.content = 'Score: ' + score;
    
    popAudio.play('',0,1,false);
}

function touchBaddy (player, baddy) {
    baddy.kill();
    
    score -= 100;
    scoreText.content = 'Score: ' + score;
    
    hitAudio.play('',0,1,false);
}